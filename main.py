import pandas as pd
import os
import datetime
import pandas_gbq

directory = "/var/data/wrf/"


def push_bq(data_frame):
    df = data_frame
    projectid = 'pluvion-tech'
    name_table = 'forecast.fc_wrf_4d_d'
    pandas_gbq.to_gbq(
        df, name_table,
        project_id=projectid,
        if_exists='append'
    )
    print('Upload: '+str(df.shape))


def read_directory():
    list_file = os.listdir(directory)
    columns = ['date', 'forecast', 'data_type',
               'surface', 'lon', 'lat', 'data']
    list_file = sorted(list_file)
    for file in list_file:
        if file.endswith(".csv"):
            print(file, ' - ', str(datetime.datetime.utcnow()))
            try:
                df = pd.read_csv(directory+file,
                                 header=None, names=columns)
                clean_data(df, file)
                os.remove(directory+file)
            except:
                continue


def clean_data(df, file_name):
    variables = ['date', 'forecast', 'surface', 'lon', 'lat',
                 'VIS', 'GUST', 'PRES', 'HGT',
                 'TMP', 'SPFH', 'WEASD', 'APCP', 'ACPCP']

    df_edit = df[['date', 'forecast', 'surface',
                  'lon', 'lat']].drop_duplicates()

    df_VIS = df[df['data_type'] == 'VIS']
    df_VIS.rename(columns={'data': 'VIS'}, inplace=True)
    result = pd.merge(df_edit, df_VIS, on=variables[0:5])
    result = result[variables[0:6]]
    df_VIS = []

    for i in range(6, len(variables)):
        name = str(variables[i])
        print("Parser: ", name)
        df_log = df[df['data_type'] == name]
        df_log.rename(columns={'data': name}, inplace=True)
        result = pd.merge(result, df_log, on=variables[0:5])
        result = result[variables[0:i+1]]
        df_log = []

    df = []  # zerando memoria

    result['date'] = pd.to_datetime(result['date'])
    result['forecast'] = pd.to_datetime(result['forecast'])

    print('save backup')
    # push_bq(result)
    result.to_csv(directory+'backup/parsed_'+file_name, index=False)


read_directory()
