import pandas as pd
import os
import datetime
import pandas_gbq
import time
from google.cloud import pubsub_v1
import json


directory = "/var/data/wrf/"

project_id = 'pluvion-tech'
topic_name = 'data_to_gbq'
name_table = 'forecast.fc_wrf_4d_d'

step = 50000


# def push_bq(data_frame):
#     df = data_frame
#     projectid = 'pluvion-tech'
#     name_table = 'forecast.fc_wrf_4d_d'
#     pandas_gbq.to_gbq(
#         df, name_table,
#         project_id=projectid,
#         if_exists='append'
#     )
#     print('Upload: '+str(df.shape))

def callback(message_future):
    if message_future.exception(timeout=30):
        print('Publishing message on {} threw an Exception {}.'.format(
            topic_name, message_future.exception()))
    else:
        print(message_future.result())


def publisher_pubsub(df):

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    df = df.drop_duplicates()
    # print(df['date'].count())

    size = df['date'].count()
    print('Published message IDs:')
    control = 0
    for i in range(0, size, step):
        time.sleep(2)
        control = control + 1
        # print(size)
        #print(i, i+step)
        df_edit = df[i:i+step]
        # print(df_edit.shape)
        df_edit = df_edit.to_json(orient='split', index=False)

        data = {
            "project_id": project_id,
            "name_table": name_table,
            "data": df_edit,
            "to_date": ['date', 'forecast']
        }
        #data = str(data).replace("'", '"')
        data = json.dumps(data)
        data = data.encode('utf-8')
        message_future = publisher.publish(topic_path, data=data)
        message_future.add_done_callback(callback)
        if(control % 100 == 0):
            time.sleep(300)


def read_directory():
    list_file = os.listdir(directory)
    columns = ['date', 'forecast', 'data_type',
               'surface', 'lon', 'lat', 'data']
    list_file = sorted(list_file,  reverse=True)
    for file in list_file:
        if file.endswith(".csv"):
            print(file, ' - ', str(datetime.datetime.utcnow()))
            try:
                df = pd.read_csv(directory+file,
                                 header=None, names=columns)
                clean_data(df, file)
                os.remove(directory+file)
            except:
                continue


def clean_data(df, file_name):
    variables = ['date', 'forecast', 'surface', 'lon', 'lat',
                 'VIS', 'GUST', 'PRES', 'HGT',
                 'TMP', 'SPFH', 'WEASD', 'APCP', 'ACPCP']

    df_edit = df[['date', 'forecast', 'surface',
                  'lon', 'lat']].drop_duplicates()

    df_VIS = df[df['data_type'] == 'VIS']
    df_VIS.rename(columns={'data': 'VIS'}, inplace=True)
    result = pd.merge(df_edit, df_VIS, on=variables[0:5])
    result = result[variables[0:6]]
    df_VIS = []

    for i in range(6, len(variables)):
        name = str(variables[i])
        print("Parser: ", name)
        df_log = df[df['data_type'] == name]
        df_log.rename(columns={'data': name}, inplace=True)
        result = pd.merge(result, df_log, on=variables[0:5])
        result = result[variables[0:i+1]]
        df_log = []

    df = []  # zerando memoria

    result['date'] = pd.to_datetime(result['date'])
    result['forecast'] = pd.to_datetime(result['forecast'])

    print('save backup')
    # push_bq(result)
    result.to_csv(directory+'backup/parsed_'+file_name, index=False)
    publisher_pubsub(result)


read_directory()
