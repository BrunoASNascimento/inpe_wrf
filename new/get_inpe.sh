
    #!/bin/bash
#
#define URL
#

cd /var/data/wrf

ano=$(date +'%Y')
mes=$(date +'%m')
dia=$(date +'%d')
hr=00

BASE_URL="http://ftp1.cptec.inpe.br/modelos/tempo/WRF/ams_05km/brutos/${ano}/${mes}/${dia}/${hr}/"

date=$(date +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 0ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 0ED_WRF_${date}${hr}_${date}f${i}.grb -csv 0ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 1ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 1ED_WRF_${date}${hr}_${date}f${i}.grb -csv 1ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 2ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 2ED_WRF_${date}${hr}_${date}f${i}.grb -csv 2ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 3ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 3ED_WRF_${date}${hr}_${date}f${i}.grb -csv 3ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 4ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 4ED_WRF_${date}${hr}_${date}f${i}.grb -csv 4ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 5ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 5ED_WRF_${date}${hr}_${date}f${i}.grb -csv 5ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 6ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 6ED_WRF_${date}${hr}_${date}f${i}.grb -csv 6ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 7ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 7ED_WRF_${date}${hr}_${date}f${i}.grb -csv 7ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 8ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 8ED_WRF_${date}${hr}_${date}f${i}.grb -csv 8ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 9ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 9ED_WRF_${date}${hr}_${date}f${i}.grb -csv 9ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 10ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 10ED_WRF_${date}${hr}_${date}f${i}.grb -csv 10ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 11ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 11ED_WRF_${date}${hr}_${date}f${i}.grb -csv 11ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 12ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 12ED_WRF_${date}${hr}_${date}f${i}.grb -csv 12ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 13ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 13ED_WRF_${date}${hr}_${date}f${i}.grb -csv 13ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 14ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 14ED_WRF_${date}${hr}_${date}f${i}.grb -csv 14ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 15ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 15ED_WRF_${date}${hr}_${date}f${i}.grb -csv 15ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 16ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 16ED_WRF_${date}${hr}_${date}f${i}.grb -csv 16ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 17ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 17ED_WRF_${date}${hr}_${date}f${i}.grb -csv 17ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 18ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 18ED_WRF_${date}${hr}_${date}f${i}.grb -csv 18ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 19ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 19ED_WRF_${date}${hr}_${date}f${i}.grb -csv 19ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 20ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 20ED_WRF_${date}${hr}_${date}f${i}.grb -csv 20ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 21ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 21ED_WRF_${date}${hr}_${date}f${i}.grb -csv 21ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 22ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 22ED_WRF_${date}${hr}_${date}f${i}.grb -csv 22ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 23ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 23ED_WRF_${date}${hr}_${date}f${i}.grb -csv 23ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 24ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 24ED_WRF_${date}${hr}_${date}f${i}.grb -csv 24ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 25ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 25ED_WRF_${date}${hr}_${date}f${i}.grb -csv 25ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 26ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 26ED_WRF_${date}${hr}_${date}f${i}.grb -csv 26ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 27ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 27ED_WRF_${date}${hr}_${date}f${i}.grb -csv 27ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 28ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 28ED_WRF_${date}${hr}_${date}f${i}.grb -csv 28ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 29ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 29ED_WRF_${date}${hr}_${date}f${i}.grb -csv 29ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 30ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 30ED_WRF_${date}${hr}_${date}f${i}.grb -csv 30ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 31ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 31ED_WRF_${date}${hr}_${date}f${i}.grb -csv 31ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 32ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 32ED_WRF_${date}${hr}_${date}f${i}.grb -csv 32ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 33ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 33ED_WRF_${date}${hr}_${date}f${i}.grb -csv 33ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 34ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 34ED_WRF_${date}${hr}_${date}f${i}.grb -csv 34ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 35ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 35ED_WRF_${date}${hr}_${date}f${i}.grb -csv 35ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 36ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 36ED_WRF_${date}${hr}_${date}f${i}.grb -csv 36ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 37ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 37ED_WRF_${date}${hr}_${date}f${i}.grb -csv 37ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 38ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 38ED_WRF_${date}${hr}_${date}f${i}.grb -csv 38ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 39ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 39ED_WRF_${date}${hr}_${date}f${i}.grb -csv 39ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 40ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 40ED_WRF_${date}${hr}_${date}f${i}.grb -csv 40ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 41ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 41ED_WRF_${date}${hr}_${date}f${i}.grb -csv 41ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 42ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 42ED_WRF_${date}${hr}_${date}f${i}.grb -csv 42ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 43ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 43ED_WRF_${date}${hr}_${date}f${i}.grb -csv 43ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 44ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 44ED_WRF_${date}${hr}_${date}f${i}.grb -csv 44ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 45ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 45ED_WRF_${date}${hr}_${date}f${i}.grb -csv 45ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 46ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 46ED_WRF_${date}${hr}_${date}f${i}.grb -csv 46ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 47ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 47ED_WRF_${date}${hr}_${date}f${i}.grb -csv 47ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 48ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 48ED_WRF_${date}${hr}_${date}f${i}.grb -csv 48ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 49ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 49ED_WRF_${date}${hr}_${date}f${i}.grb -csv 49ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 50ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 50ED_WRF_${date}${hr}_${date}f${i}.grb -csv 50ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 51ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 51ED_WRF_${date}${hr}_${date}f${i}.grb -csv 51ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 52ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 52ED_WRF_${date}${hr}_${date}f${i}.grb -csv 52ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 53ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 53ED_WRF_${date}${hr}_${date}f${i}.grb -csv 53ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 54ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 54ED_WRF_${date}${hr}_${date}f${i}.grb -csv 54ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 55ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 55ED_WRF_${date}${hr}_${date}f${i}.grb -csv 55ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 56ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 56ED_WRF_${date}${hr}_${date}f${i}.grb -csv 56ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 57ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 57ED_WRF_${date}${hr}_${date}f${i}.grb -csv 57ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 58ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 58ED_WRF_${date}${hr}_${date}f${i}.grb -csv 58ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 59ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 59ED_WRF_${date}${hr}_${date}f${i}.grb -csv 59ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 60ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 60ED_WRF_${date}${hr}_${date}f${i}.grb -csv 60ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 61ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 61ED_WRF_${date}${hr}_${date}f${i}.grb -csv 61ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 62ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 62ED_WRF_${date}${hr}_${date}f${i}.grb -csv 62ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 63ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 63ED_WRF_${date}${hr}_${date}f${i}.grb -csv 63ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 64ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 64ED_WRF_${date}${hr}_${date}f${i}.grb -csv 64ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 65ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 65ED_WRF_${date}${hr}_${date}f${i}.grb -csv 65ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 66ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 66ED_WRF_${date}${hr}_${date}f${i}.grb -csv 66ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 67ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 67ED_WRF_${date}${hr}_${date}f${i}.grb -csv 67ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 68ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 68ED_WRF_${date}${hr}_${date}f${i}.grb -csv 68ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 69ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 69ED_WRF_${date}${hr}_${date}f${i}.grb -csv 69ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 70ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 70ED_WRF_${date}${hr}_${date}f${i}.grb -csv 70ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 71ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 71ED_WRF_${date}${hr}_${date}f${i}.grb -csv 71ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 72ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 72ED_WRF_${date}${hr}_${date}f${i}.grb -csv 72ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 73ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 73ED_WRF_${date}${hr}_${date}f${i}.grb -csv 73ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 74ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 74ED_WRF_${date}${hr}_${date}f${i}.grb -csv 74ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 75ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 75ED_WRF_${date}${hr}_${date}f${i}.grb -csv 75ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 76ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 76ED_WRF_${date}${hr}_${date}f${i}.grb -csv 76ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 77ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 77ED_WRF_${date}${hr}_${date}f${i}.grb -csv 77ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 78ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 78ED_WRF_${date}${hr}_${date}f${i}.grb -csv 78ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 79ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 79ED_WRF_${date}${hr}_${date}f${i}.grb -csv 79ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 80ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 80ED_WRF_${date}${hr}_${date}f${i}.grb -csv 80ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 81ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 81ED_WRF_${date}${hr}_${date}f${i}.grb -csv 81ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 82ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 82ED_WRF_${date}${hr}_${date}f${i}.grb -csv 82ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 83ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 83ED_WRF_${date}${hr}_${date}f${i}.grb -csv 83ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 84ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 84ED_WRF_${date}${hr}_${date}f${i}.grb -csv 84ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 85ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 85ED_WRF_${date}${hr}_${date}f${i}.grb -csv 85ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 86ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 86ED_WRF_${date}${hr}_${date}f${i}.grb -csv 86ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 87ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 87ED_WRF_${date}${hr}_${date}f${i}.grb -csv 87ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 88ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 88ED_WRF_${date}${hr}_${date}f${i}.grb -csv 88ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 89ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 89ED_WRF_${date}${hr}_${date}f${i}.grb -csv 89ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 90ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 90ED_WRF_${date}${hr}_${date}f${i}.grb -csv 90ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 91ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 91ED_WRF_${date}${hr}_${date}f${i}.grb -csv 91ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 92ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 92ED_WRF_${date}${hr}_${date}f${i}.grb -csv 92ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 93ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 93ED_WRF_${date}${hr}_${date}f${i}.grb -csv 93ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 94ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 94ED_WRF_${date}${hr}_${date}f${i}.grb -csv 94ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 95ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 95ED_WRF_${date}${hr}_${date}f${i}.grb -csv 95ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 96ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 96ED_WRF_${date}${hr}_${date}f${i}.grb -csv 96ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 97ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 97ED_WRF_${date}${hr}_${date}f${i}.grb -csv 97ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 98ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 98ED_WRF_${date}${hr}_${date}f${i}.grb -csv 98ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 99ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 99ED_WRF_${date}${hr}_${date}f${i}.grb -csv 99ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 100ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 100ED_WRF_${date}${hr}_${date}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 101ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 101ED_WRF_${date}${hr}_${date}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 102ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 102ED_WRF_${date}${hr}_${date}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 103ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 103ED_WRF_${date}${hr}_${date}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 104ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 104ED_WRF_${date}${hr}_${date}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 105ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 105ED_WRF_${date}${hr}_${date}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 106ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 106ED_WRF_${date}${hr}_${date}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 107ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 107ED_WRF_${date}${hr}_${date}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -38.5:1:1  -12.96:1:1 108ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 108ED_WRF_${date}${hr}_${date}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 109ED_WRF_${date}${hr}_${date}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date}f${i}.grb grib
 wgrib2 110ED_WRF_${date}${hr}_${date}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date}f${i}.csv
rm *ED_WRF*
done

python3 /var/scripts/inpe_wrf/main_normal.py &
python3 /var/scripts/inpe_wrf/main_reverse.py &


date2=$(date --date="+1 day" +'%Y%m%d')
echo "$date2"
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date2}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date2}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 0ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 0ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 0ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 1ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 1ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 1ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 2ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 2ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 2ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 3ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 3ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 3ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 4ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 4ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 4ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 5ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 5ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 5ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 6ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 6ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 6ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 7ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 7ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 7ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 8ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 8ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 8ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 9ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 9ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 9ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 10ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 10ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 10ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 11ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 11ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 11ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 12ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 12ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 12ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 13ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 13ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 13ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 14ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 14ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 14ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 15ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 15ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 15ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 16ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 16ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 16ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 17ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 17ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 17ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 18ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 18ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 18ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 19ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 19ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 19ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 20ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 20ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 20ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 21ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 21ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 21ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 22ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 22ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 22ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 23ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 23ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 23ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 24ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 24ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 24ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 25ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 25ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 25ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 26ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 26ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 26ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 27ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 27ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 27ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 28ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 28ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 28ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 29ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 29ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 29ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 30ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 30ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 30ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 31ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 31ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 31ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 32ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 32ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 32ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 33ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 33ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 33ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 34ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 34ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 34ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 35ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 35ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 35ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 36ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 36ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 36ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 37ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 37ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 37ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 38ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 38ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 38ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 39ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 39ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 39ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 40ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 40ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 40ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 41ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 41ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 41ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 42ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 42ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 42ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 43ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 43ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 43ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 44ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 44ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 44ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 45ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 45ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 45ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 46ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 46ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 46ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 47ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 47ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 47ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 48ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 48ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 48ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 49ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 49ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 49ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 50ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 50ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 50ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 51ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 51ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 51ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 52ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 52ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 52ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 53ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 53ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 53ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 54ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 54ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 54ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 55ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 55ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 55ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 56ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 56ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 56ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 57ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 57ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 57ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 58ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 58ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 58ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 59ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 59ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 59ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 60ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 60ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 60ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 61ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 61ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 61ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 62ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 62ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 62ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 63ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 63ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 63ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 64ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 64ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 64ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 65ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 65ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 65ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 66ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 66ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 66ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 67ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 67ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 67ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 68ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 68ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 68ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 69ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 69ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 69ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 70ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 70ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 70ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 71ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 71ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 71ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 72ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 72ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 72ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 73ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 73ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 73ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 74ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 74ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 74ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 75ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 75ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 75ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 76ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 76ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 76ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 77ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 77ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 77ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 78ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 78ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 78ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 79ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 79ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 79ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 80ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 80ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 80ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 81ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 81ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 81ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 82ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 82ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 82ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 83ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 83ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 83ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 84ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 84ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 84ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 85ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 85ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 85ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 86ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 86ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 86ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 87ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 87ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 87ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 88ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 88ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 88ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 89ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 89ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 89ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 90ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 90ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 90ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 91ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 91ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 91ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 92ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 92ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 92ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 93ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 93ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 93ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 94ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 94ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 94ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 95ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 95ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 95ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 96ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 96ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 96ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 97ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 97ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 97ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 98ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 98ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 98ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 99ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 99ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 99ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 100ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 100ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 101ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 101ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 102ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 102ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 103ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 103ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 104ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 104ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 105ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 105ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 106ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 106ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 107ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 107ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -38.5:1:1  -12.96:1:1 108ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 108ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 109ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date2}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date2}f${i}.grb grib
 wgrib2 110ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date2}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date2}f${i}.csv
rm *ED_WRF*
done


date3=$(date --date="+2 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date3}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date3}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 0ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 0ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 0ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 1ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 1ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 1ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 2ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 2ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 2ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 3ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 3ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 3ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 4ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 4ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 4ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 5ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 5ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 5ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 6ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 6ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 6ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 7ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 7ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 7ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 8ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 8ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 8ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 9ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 9ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 9ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 10ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 10ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 10ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 11ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 11ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 11ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 12ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 12ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 12ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 13ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 13ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 13ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 14ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 14ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 14ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 15ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 15ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 15ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 16ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 16ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 16ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 17ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 17ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 17ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 18ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 18ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 18ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 19ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 19ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 19ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 20ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 20ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 20ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 21ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 21ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 21ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 22ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 22ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 22ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 23ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 23ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 23ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 24ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 24ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 24ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 25ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 25ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 25ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 26ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 26ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 26ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 27ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 27ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 27ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 28ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 28ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 28ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 29ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 29ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 29ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 30ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 30ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 30ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 31ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 31ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 31ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 32ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 32ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 32ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 33ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 33ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 33ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 34ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 34ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 34ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 35ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 35ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 35ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 36ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 36ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 36ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 37ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 37ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 37ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 38ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 38ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 38ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 39ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 39ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 39ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 40ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 40ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 40ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 41ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 41ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 41ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 42ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 42ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 42ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 43ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 43ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 43ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 44ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 44ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 44ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 45ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 45ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 45ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 46ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 46ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 46ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 47ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 47ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 47ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 48ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 48ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 48ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 49ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 49ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 49ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 50ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 50ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 50ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 51ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 51ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 51ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 52ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 52ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 52ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 53ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 53ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 53ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 54ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 54ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 54ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 55ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 55ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 55ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 56ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 56ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 56ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 57ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 57ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 57ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 58ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 58ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 58ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 59ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 59ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 59ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 60ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 60ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 60ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 61ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 61ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 61ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 62ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 62ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 62ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 63ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 63ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 63ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 64ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 64ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 64ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 65ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 65ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 65ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 66ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 66ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 66ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 67ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 67ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 67ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 68ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 68ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 68ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 69ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 69ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 69ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 70ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 70ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 70ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 71ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 71ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 71ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 72ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 72ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 72ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 73ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 73ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 73ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 74ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 74ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 74ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 75ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 75ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 75ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 76ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 76ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 76ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 77ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 77ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 77ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 78ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 78ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 78ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 79ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 79ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 79ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 80ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 80ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 80ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 81ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 81ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 81ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 82ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 82ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 82ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 83ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 83ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 83ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 84ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 84ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 84ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 85ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 85ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 85ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 86ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 86ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 86ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 87ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 87ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 87ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 88ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 88ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 88ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 89ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 89ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 89ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 90ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 90ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 90ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 91ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 91ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 91ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 92ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 92ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 92ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 93ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 93ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 93ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 94ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 94ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 94ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 95ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 95ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 95ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 96ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 96ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 96ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 97ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 97ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 97ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 98ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 98ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 98ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 99ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 99ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 99ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 100ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 100ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 101ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 101ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 102ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 102ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 103ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 103ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 104ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 104ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 105ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 105ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 106ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 106ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 107ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 107ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -38.5:1:1  -12.96:1:1 108ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 108ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 109ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date3}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date3}f${i}.grb grib
 wgrib2 110ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date3}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date3}f${i}.csv
rm *ED_WRF*
done


date4=$(date --date="+3 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date4}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date4}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 0ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 0ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 0ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 1ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 1ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 1ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 2ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 2ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 2ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 3ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 3ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 3ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 4ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 4ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 4ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 5ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 5ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 5ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 6ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 6ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 6ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 7ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 7ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 7ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 8ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 8ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 8ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 9ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 9ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 9ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 10ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 10ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 10ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 11ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 11ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 11ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 12ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 12ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 12ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 13ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 13ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 13ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 14ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 14ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 14ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 15ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 15ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 15ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 16ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 16ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 16ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 17ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 17ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 17ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 18ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 18ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 18ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 19ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 19ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 19ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 20ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 20ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 20ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 21ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 21ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 21ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 22ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 22ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 22ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 23ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 23ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 23ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 24ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 24ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 24ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 25ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 25ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 25ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 26ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 26ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 26ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 27ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 27ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 27ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 28ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 28ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 28ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 29ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 29ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 29ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 30ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 30ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 30ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 31ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 31ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 31ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 32ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 32ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 32ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 33ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 33ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 33ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 34ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 34ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 34ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 35ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 35ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 35ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 36ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 36ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 36ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 37ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 37ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 37ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 38ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 38ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 38ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 39ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 39ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 39ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 40ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 40ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 40ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 41ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 41ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 41ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 42ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 42ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 42ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 43ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 43ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 43ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 44ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 44ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 44ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 45ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 45ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 45ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 46ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 46ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 46ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 47ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 47ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 47ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 48ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 48ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 48ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 49ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 49ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 49ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 50ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 50ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 50ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 51ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 51ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 51ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 52ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 52ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 52ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 53ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 53ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 53ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 54ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 54ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 54ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 55ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 55ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 55ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 56ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 56ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 56ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 57ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 57ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 57ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 58ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 58ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 58ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 59ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 59ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 59ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 60ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 60ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 60ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 61ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 61ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 61ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 62ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 62ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 62ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 63ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 63ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 63ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 64ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 64ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 64ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 65ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 65ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 65ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 66ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 66ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 66ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 67ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 67ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 67ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 68ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 68ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 68ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 69ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 69ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 69ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 70ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 70ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 70ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 71ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 71ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 71ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 72ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 72ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 72ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 73ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 73ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 73ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 74ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 74ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 74ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 75ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 75ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 75ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 76ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 76ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 76ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 77ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 77ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 77ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 78ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 78ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 78ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 79ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 79ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 79ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 80ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 80ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 80ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 81ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 81ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 81ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 82ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 82ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 82ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 83ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 83ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 83ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 84ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 84ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 84ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 85ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 85ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 85ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 86ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 86ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 86ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 87ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 87ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 87ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 88ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 88ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 88ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 89ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 89ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 89ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 90ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 90ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 90ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 91ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 91ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 91ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 92ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 92ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 92ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 93ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 93ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 93ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 94ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 94ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 94ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 95ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 95ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 95ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 96ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 96ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 96ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 97ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 97ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 97ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 98ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 98ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 98ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 99ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 99ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 99ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 100ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 100ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 101ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 101ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 102ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 102ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 103ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 103ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 104ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 104ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 105ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 105ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 106ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 106ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 107ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 107ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -38.5:1:1  -12.96:1:1 108ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 108ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 109ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date4}f${i}.csv &
wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date4}f${i}.grb grib
 wgrib2 110ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date4}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date4}f${i}.csv
rm *ED_WRF*
done

python3 /var/scripts/inpe_wrf/main_normal.py &
python3 /var/scripts/inpe_wrf/main_reverse.py &

    