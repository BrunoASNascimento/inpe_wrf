cd /var/data/wrf/backup/

python3 /var/scripts/inpe_wrf/persister_upload_pubsub.py &
python3 /var/scripts/inpe_wrf/persister_upload_pubsub_reverse.py

for i in *.csv; do
    echo ${i} &
    #bq --location=US load --autodetect --noreplace --source_format=CSV forecast.fc_wrf_4d_d ${i} &
    tar -czf csv_${i}.tar.gz ${i}
    #gsutil cp ${i}.gz gs://pluvion_forecasts/GFS
    gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 csv_${i}.tar.gz
    rm ${i}* &
    rm csv_${i}.tar.gz
done
