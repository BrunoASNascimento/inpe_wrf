import pandas_gbq

sql = """SELECT distinct latitude_wrf, longitude_wrf FROM `pluvion-tech.stations.all_stations` order by 1,2"""
df1 = pandas_gbq.read_gbq(sql, project_id='pluvion-tech')


sql = """SELECT  lon,  lat FROM  `pluvion-tech.forecast.fc_wrf_4d_d` WHERE  DATE(date)=DATE('2020-03-10') GROUP BY  1, 2 ORDER BY  1,  2"""
df2 = pandas_gbq.read_gbq(sql, project_id='pluvion-tech')

listalat1 = []
listalon1 = []
for index, row in df1.iterrows():
    listalat1.append(row['latitude_wrf'])
    listalon1.append(row['longitude_wrf'])

listalat2 = []
listalon2 = []
for index, row in df2.iterrows():
    listalat2.append(row['lat'])
    listalon2.append(row['lon'])

print("Missing latitudes:", (set(listalat1).difference(listalat2))) 
print("Missing longitudes:", (set(listalon1).difference(listalon2))) 
