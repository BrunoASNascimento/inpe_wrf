
    #!/bin/bash
#
#define URL
#

cd /var/data/wrf

ano=$(date +'%Y')
mes=$(date +'%m')
dia=$(date +'%d')
hr=00

BASE_URL="http://ftp1.cptec.inpe.br/modelos/tempo/WRF/ams_05km/brutos/${ano}/${mes}/${dia}/${hr}/"

date=$(date +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
       wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 001ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 001ED_WRF_${date}${hr}_${date}f${i}.grb -csv 001ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 002ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 002ED_WRF_${date}${hr}_${date}f${i}.grb -csv 002ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 003ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 003ED_WRF_${date}${hr}_${date}f${i}.grb -csv 003ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 004ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 004ED_WRF_${date}${hr}_${date}f${i}.grb -csv 004ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 005ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 005ED_WRF_${date}${hr}_${date}f${i}.grb -csv 005ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 006ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 006ED_WRF_${date}${hr}_${date}f${i}.grb -csv 006ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 007ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 007ED_WRF_${date}${hr}_${date}f${i}.grb -csv 007ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 008ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 008ED_WRF_${date}${hr}_${date}f${i}.grb -csv 008ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 009ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 009ED_WRF_${date}${hr}_${date}f${i}.grb -csv 009ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 010ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 010ED_WRF_${date}${hr}_${date}f${i}.grb -csv 010ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 011ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 011ED_WRF_${date}${hr}_${date}f${i}.grb -csv 011ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 012ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 012ED_WRF_${date}${hr}_${date}f${i}.grb -csv 012ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 013ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 013ED_WRF_${date}${hr}_${date}f${i}.grb -csv 013ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 014ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 014ED_WRF_${date}${hr}_${date}f${i}.grb -csv 014ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 015ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 015ED_WRF_${date}${hr}_${date}f${i}.grb -csv 015ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 016ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 016ED_WRF_${date}${hr}_${date}f${i}.grb -csv 016ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 017ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 017ED_WRF_${date}${hr}_${date}f${i}.grb -csv 017ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 018ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 018ED_WRF_${date}${hr}_${date}f${i}.grb -csv 018ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 019ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 019ED_WRF_${date}${hr}_${date}f${i}.grb -csv 019ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 020ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 020ED_WRF_${date}${hr}_${date}f${i}.grb -csv 020ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 021ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 021ED_WRF_${date}${hr}_${date}f${i}.grb -csv 021ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 022ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 022ED_WRF_${date}${hr}_${date}f${i}.grb -csv 022ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 023ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 023ED_WRF_${date}${hr}_${date}f${i}.grb -csv 023ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 024ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 024ED_WRF_${date}${hr}_${date}f${i}.grb -csv 024ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 025ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 025ED_WRF_${date}${hr}_${date}f${i}.grb -csv 025ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 026ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 026ED_WRF_${date}${hr}_${date}f${i}.grb -csv 026ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 027ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 027ED_WRF_${date}${hr}_${date}f${i}.grb -csv 027ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 028ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 028ED_WRF_${date}${hr}_${date}f${i}.grb -csv 028ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 029ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 029ED_WRF_${date}${hr}_${date}f${i}.grb -csv 029ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 030ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 030ED_WRF_${date}${hr}_${date}f${i}.grb -csv 030ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 031ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 031ED_WRF_${date}${hr}_${date}f${i}.grb -csv 031ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 032ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 032ED_WRF_${date}${hr}_${date}f${i}.grb -csv 032ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 033ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 033ED_WRF_${date}${hr}_${date}f${i}.grb -csv 033ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 034ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 034ED_WRF_${date}${hr}_${date}f${i}.grb -csv 034ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 035ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 035ED_WRF_${date}${hr}_${date}f${i}.grb -csv 035ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 036ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 036ED_WRF_${date}${hr}_${date}f${i}.grb -csv 036ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 037ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 037ED_WRF_${date}${hr}_${date}f${i}.grb -csv 037ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 038ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 038ED_WRF_${date}${hr}_${date}f${i}.grb -csv 038ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 039ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 039ED_WRF_${date}${hr}_${date}f${i}.grb -csv 039ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 040ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 040ED_WRF_${date}${hr}_${date}f${i}.grb -csv 040ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 041ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 041ED_WRF_${date}${hr}_${date}f${i}.grb -csv 041ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 042ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 042ED_WRF_${date}${hr}_${date}f${i}.grb -csv 042ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 043ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 043ED_WRF_${date}${hr}_${date}f${i}.grb -csv 043ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 044ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 044ED_WRF_${date}${hr}_${date}f${i}.grb -csv 044ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 045ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 045ED_WRF_${date}${hr}_${date}f${i}.grb -csv 045ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 046ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 046ED_WRF_${date}${hr}_${date}f${i}.grb -csv 046ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 047ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 047ED_WRF_${date}${hr}_${date}f${i}.grb -csv 047ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 048ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 048ED_WRF_${date}${hr}_${date}f${i}.grb -csv 048ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 049ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 049ED_WRF_${date}${hr}_${date}f${i}.grb -csv 049ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 050ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 050ED_WRF_${date}${hr}_${date}f${i}.grb -csv 050ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 051ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 051ED_WRF_${date}${hr}_${date}f${i}.grb -csv 051ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 052ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 052ED_WRF_${date}${hr}_${date}f${i}.grb -csv 052ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 053ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 053ED_WRF_${date}${hr}_${date}f${i}.grb -csv 053ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 054ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 054ED_WRF_${date}${hr}_${date}f${i}.grb -csv 054ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 055ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 055ED_WRF_${date}${hr}_${date}f${i}.grb -csv 055ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 056ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 056ED_WRF_${date}${hr}_${date}f${i}.grb -csv 056ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 057ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 057ED_WRF_${date}${hr}_${date}f${i}.grb -csv 057ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 058ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 058ED_WRF_${date}${hr}_${date}f${i}.grb -csv 058ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 059ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 059ED_WRF_${date}${hr}_${date}f${i}.grb -csv 059ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 060ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 060ED_WRF_${date}${hr}_${date}f${i}.grb -csv 060ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 061ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 061ED_WRF_${date}${hr}_${date}f${i}.grb -csv 061ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 062ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 062ED_WRF_${date}${hr}_${date}f${i}.grb -csv 062ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 063ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 063ED_WRF_${date}${hr}_${date}f${i}.grb -csv 063ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 064ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 064ED_WRF_${date}${hr}_${date}f${i}.grb -csv 064ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 065ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 065ED_WRF_${date}${hr}_${date}f${i}.grb -csv 065ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 066ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 066ED_WRF_${date}${hr}_${date}f${i}.grb -csv 066ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 067ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 067ED_WRF_${date}${hr}_${date}f${i}.grb -csv 067ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 068ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 068ED_WRF_${date}${hr}_${date}f${i}.grb -csv 068ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 069ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 069ED_WRF_${date}${hr}_${date}f${i}.grb -csv 069ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 070ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 070ED_WRF_${date}${hr}_${date}f${i}.grb -csv 070ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 071ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 071ED_WRF_${date}${hr}_${date}f${i}.grb -csv 071ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 072ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 072ED_WRF_${date}${hr}_${date}f${i}.grb -csv 072ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 073ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 073ED_WRF_${date}${hr}_${date}f${i}.grb -csv 073ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 074ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 074ED_WRF_${date}${hr}_${date}f${i}.grb -csv 074ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 075ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 075ED_WRF_${date}${hr}_${date}f${i}.grb -csv 075ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 076ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 076ED_WRF_${date}${hr}_${date}f${i}.grb -csv 076ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 077ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 077ED_WRF_${date}${hr}_${date}f${i}.grb -csv 077ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 078ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 078ED_WRF_${date}${hr}_${date}f${i}.grb -csv 078ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 079ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 079ED_WRF_${date}${hr}_${date}f${i}.grb -csv 079ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 080ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 080ED_WRF_${date}${hr}_${date}f${i}.grb -csv 080ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 081ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 081ED_WRF_${date}${hr}_${date}f${i}.grb -csv 081ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 082ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 082ED_WRF_${date}${hr}_${date}f${i}.grb -csv 082ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 083ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 083ED_WRF_${date}${hr}_${date}f${i}.grb -csv 083ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 084ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 084ED_WRF_${date}${hr}_${date}f${i}.grb -csv 084ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 085ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 085ED_WRF_${date}${hr}_${date}f${i}.grb -csv 085ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 086ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 086ED_WRF_${date}${hr}_${date}f${i}.grb -csv 086ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 087ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 087ED_WRF_${date}${hr}_${date}f${i}.grb -csv 087ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 088ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 088ED_WRF_${date}${hr}_${date}f${i}.grb -csv 088ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 089ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 089ED_WRF_${date}${hr}_${date}f${i}.grb -csv 089ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 090ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 090ED_WRF_${date}${hr}_${date}f${i}.grb -csv 090ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 091ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 091ED_WRF_${date}${hr}_${date}f${i}.grb -csv 091ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 092ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 092ED_WRF_${date}${hr}_${date}f${i}.grb -csv 092ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 093ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 093ED_WRF_${date}${hr}_${date}f${i}.grb -csv 093ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 094ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 094ED_WRF_${date}${hr}_${date}f${i}.grb -csv 094ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 095ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 095ED_WRF_${date}${hr}_${date}f${i}.grb -csv 095ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 096ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 096ED_WRF_${date}${hr}_${date}f${i}.grb -csv 096ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 097ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 097ED_WRF_${date}${hr}_${date}f${i}.grb -csv 097ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 098ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 098ED_WRF_${date}${hr}_${date}f${i}.grb -csv 098ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 099ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 099ED_WRF_${date}${hr}_${date}f${i}.grb -csv 099ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 100ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 100ED_WRF_${date}${hr}_${date}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 101ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 101ED_WRF_${date}${hr}_${date}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 102ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 102ED_WRF_${date}${hr}_${date}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 103ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 103ED_WRF_${date}${hr}_${date}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 104ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 104ED_WRF_${date}${hr}_${date}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 105ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 105ED_WRF_${date}${hr}_${date}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 106ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 106ED_WRF_${date}${hr}_${date}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 107ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 107ED_WRF_${date}${hr}_${date}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 108ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 108ED_WRF_${date}${hr}_${date}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 109ED_WRF_${date}${hr}_${date}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 110ED_WRF_${date}${hr}_${date}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.44:1:1 111ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 111ED_WRF_${date}${hr}_${date}f${i}.grb -csv 111ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.3:1:1 112ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 112ED_WRF_${date}${hr}_${date}f${i}.grb -csv 112ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.76:1:1  -3.3:1:1 113ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 113ED_WRF_${date}${hr}_${date}f${i}.grb -csv 113ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.74:1:1  -3.28:1:1 114ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 114ED_WRF_${date}${hr}_${date}f${i}.grb -csv 114ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.18:1:1 115ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 115ED_WRF_${date}${hr}_${date}f${i}.grb -csv 115ED_WRF_${date}${hr}_${date}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.12:1:1 116ED_WRF_${date}${hr}_${date}f${i}.grb grib
      wgrib2 116ED_WRF_${date}${hr}_${date}f${i}.grb -csv 116ED_WRF_${date}${hr}_${date}f${i}.csv &

rm *.grb 
rm *.grib2
done


date2=$(date --date="+1 day" +'%Y%m%d')
echo "$date2"
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date2}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date2}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
       wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 001ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 001ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 001ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 002ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 002ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 002ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 003ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 003ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 003ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 004ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 004ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 004ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 005ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 005ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 005ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 006ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 006ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 006ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 007ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 007ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 007ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 008ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 008ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 008ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 009ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 009ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 009ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 010ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 010ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 010ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 011ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 011ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 011ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 012ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 012ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 012ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 013ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 013ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 013ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 014ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 014ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 014ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 015ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 015ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 015ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 016ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 016ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 016ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 017ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 017ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 017ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 018ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 018ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 018ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 019ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 019ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 019ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 020ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 020ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 020ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 021ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 021ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 021ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 022ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 022ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 022ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 023ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 023ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 023ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 024ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 024ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 024ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 025ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 025ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 025ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 026ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 026ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 026ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 027ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 027ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 027ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 028ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 028ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 028ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 029ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 029ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 029ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 030ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 030ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 030ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 031ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 031ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 031ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 032ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 032ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 032ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 033ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 033ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 033ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 034ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 034ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 034ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 035ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 035ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 035ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 036ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 036ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 036ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 037ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 037ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 037ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 038ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 038ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 038ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 039ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 039ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 039ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 040ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 040ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 040ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 041ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 041ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 041ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 042ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 042ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 042ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 043ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 043ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 043ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 044ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 044ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 044ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 045ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 045ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 045ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 046ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 046ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 046ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 047ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 047ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 047ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 048ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 048ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 048ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 049ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 049ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 049ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 050ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 050ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 050ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 051ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 051ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 051ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 052ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 052ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 052ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 053ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 053ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 053ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 054ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 054ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 054ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 055ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 055ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 055ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 056ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 056ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 056ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 057ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 057ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 057ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 058ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 058ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 058ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 059ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 059ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 059ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 060ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 060ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 060ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 061ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 061ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 061ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 062ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 062ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 062ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 063ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 063ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 063ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 064ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 064ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 064ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 065ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 065ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 065ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 066ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 066ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 066ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 067ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 067ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 067ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 068ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 068ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 068ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 069ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 069ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 069ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 070ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 070ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 070ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 071ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 071ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 071ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 072ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 072ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 072ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 073ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 073ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 073ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 074ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 074ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 074ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 075ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 075ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 075ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 076ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 076ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 076ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 077ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 077ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 077ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 078ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 078ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 078ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 079ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 079ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 079ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 080ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 080ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 080ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 081ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 081ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 081ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 082ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 082ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 082ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 083ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 083ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 083ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 084ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 084ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 084ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 085ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 085ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 085ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 086ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 086ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 086ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 087ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 087ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 087ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 088ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 088ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 088ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 089ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 089ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 089ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 090ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 090ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 090ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 091ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 091ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 091ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 092ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 092ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 092ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 093ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 093ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 093ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 094ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 094ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 094ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 095ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 095ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 095ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 096ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 096ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 096ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 097ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 097ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 097ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 098ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 098ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 098ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 099ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 099ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 099ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 100ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 100ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 101ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 101ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 102ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 102ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 103ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 103ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 104ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 104ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 105ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 105ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 106ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 106ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 107ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 107ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 108ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 108ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 109ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 110ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.44:1:1 111ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 111ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 111ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.3:1:1 112ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 112ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 112ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.76:1:1  -3.3:1:1 113ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 113ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 113ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.74:1:1  -3.28:1:1 114ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 114ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 114ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.18:1:1 115ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 115ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 115ED_WRF_${date}${hr}_${date2}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.12:1:1 116ED_WRF_${date}${hr}_${date2}f${i}.grb grib
      wgrib2 116ED_WRF_${date}${hr}_${date2}f${i}.grb -csv 116ED_WRF_${date}${hr}_${date2}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date2}f${i}.csv
rm *ED_WRF*
done


date3=$(date --date="+2 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date3}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date3}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
       wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 001ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 001ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 001ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 002ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 002ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 002ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 003ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 003ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 003ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 004ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 004ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 004ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 005ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 005ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 005ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 006ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 006ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 006ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 007ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 007ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 007ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 008ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 008ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 008ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 009ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 009ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 009ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 010ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 010ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 010ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 011ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 011ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 011ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 012ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 012ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 012ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 013ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 013ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 013ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 014ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 014ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 014ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 015ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 015ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 015ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 016ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 016ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 016ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 017ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 017ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 017ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 018ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 018ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 018ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 019ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 019ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 019ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 020ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 020ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 020ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 021ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 021ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 021ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 022ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 022ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 022ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 023ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 023ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 023ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 024ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 024ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 024ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 025ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 025ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 025ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 026ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 026ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 026ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 027ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 027ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 027ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 028ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 028ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 028ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 029ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 029ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 029ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 030ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 030ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 030ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 031ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 031ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 031ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 032ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 032ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 032ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 033ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 033ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 033ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 034ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 034ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 034ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 035ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 035ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 035ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 036ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 036ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 036ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 037ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 037ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 037ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 038ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 038ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 038ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 039ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 039ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 039ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 040ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 040ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 040ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 041ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 041ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 041ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 042ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 042ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 042ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 043ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 043ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 043ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 044ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 044ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 044ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 045ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 045ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 045ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 046ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 046ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 046ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 047ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 047ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 047ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 048ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 048ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 048ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 049ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 049ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 049ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 050ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 050ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 050ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 051ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 051ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 051ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 052ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 052ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 052ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 053ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 053ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 053ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 054ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 054ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 054ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 055ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 055ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 055ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 056ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 056ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 056ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 057ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 057ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 057ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 058ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 058ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 058ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 059ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 059ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 059ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 060ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 060ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 060ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 061ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 061ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 061ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 062ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 062ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 062ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 063ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 063ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 063ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 064ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 064ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 064ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 065ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 065ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 065ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 066ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 066ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 066ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 067ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 067ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 067ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 068ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 068ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 068ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 069ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 069ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 069ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 070ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 070ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 070ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 071ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 071ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 071ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 072ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 072ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 072ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 073ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 073ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 073ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 074ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 074ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 074ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 075ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 075ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 075ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 076ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 076ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 076ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 077ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 077ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 077ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 078ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 078ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 078ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 079ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 079ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 079ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 080ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 080ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 080ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 081ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 081ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 081ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 082ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 082ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 082ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 083ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 083ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 083ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 084ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 084ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 084ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 085ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 085ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 085ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 086ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 086ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 086ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 087ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 087ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 087ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 088ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 088ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 088ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 089ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 089ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 089ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 090ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 090ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 090ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 091ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 091ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 091ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 092ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 092ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 092ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 093ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 093ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 093ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 094ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 094ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 094ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 095ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 095ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 095ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 096ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 096ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 096ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 097ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 097ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 097ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 098ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 098ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 098ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 099ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 099ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 099ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 100ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 100ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 101ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 101ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 102ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 102ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 103ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 103ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 104ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 104ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 105ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 105ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 106ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 106ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 107ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 107ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 108ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 108ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 109ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 110ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.44:1:1 111ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 111ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 111ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.3:1:1 112ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 112ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 112ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.76:1:1  -3.3:1:1 113ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 113ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 113ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.74:1:1  -3.28:1:1 114ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 114ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 114ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.18:1:1 115ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 115ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 115ED_WRF_${date}${hr}_${date3}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.12:1:1 116ED_WRF_${date}${hr}_${date3}f${i}.grb grib
      wgrib2 116ED_WRF_${date}${hr}_${date3}f${i}.grb -csv 116ED_WRF_${date}${hr}_${date3}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date3}f${i}.csv
rm *ED_WRF*
done


date4=$(date --date="+3 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date4}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date4}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
       wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.18:1:1  -29.84:1:1 001ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 001ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 001ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -52.38:1:1  -28.24:1:1 002ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 002ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 002ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.26:1:1  -27.36:1:1 003ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 003ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 003ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.0:1:1  -27.0:1:1 004ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 004ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 004ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.62:1:1  -25.48:1:1 005ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 005ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 005ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.24:1:1  -25.46:1:1 006ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 006ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 006ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.5:1:1  -25.44:1:1 007ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 007ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 007ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.46:1:1  -25.44:1:1 008ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 008ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 008ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.26:1:1  -24.0:1:1 009ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 009ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 009ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.98:1:1  -24.0:1:1 010ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 010ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 010ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.96:1:1 011ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 011ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 011ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.54:1:1  -23.96:1:1 012ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 012ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 012ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.94:1:1 013ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 013ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 013ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.94:1:1 014ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 014ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 014ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.32:1:1  -23.94:1:1 015ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 015ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 015ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.64:1:1  -23.92:1:1 016ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 016ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 016ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.88:1:1 017ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 017ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 017ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -23.84:1:1 018ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 018ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 018ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.92:1:1  -23.78:1:1 019ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 019ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 019ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.58:1:1  -23.7:1:1 020ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 020ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 020ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.66:1:1 021ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 021ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 021ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.64:1:1 022ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 022ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 022ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.62:1:1 023ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 023ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 023ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.46:1:1  -23.6:1:1 024ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 024ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 024ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.44:1:1  -23.6:1:1 025ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 025ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 025ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.16:1:1  -23.58:1:1 026ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 026ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 026ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.56:1:1 027ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 027ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 027ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.56:1:1 028ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 028ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 028ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.56:1:1 029ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 029ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 029ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.52:1:1  -23.56:1:1 030ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 030ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 030ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.54:1:1 031ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 031ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 031ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.54:1:1 032ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 032ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 032ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.54:1:1 033ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 033ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 033ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.54:1:1 034ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 034ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 034ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.54:1:1 035ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 035ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 035ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.8:1:1  -23.52:1:1 036ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 036ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 036ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.78:1:1  -23.52:1:1 037ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 037ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 037ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.52:1:1 038ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 038ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 038ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.72:1:1  -23.52:1:1 039ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 039ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 039ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.52:1:1 040ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 040ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 040ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.68:1:1  -23.52:1:1 041ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 041ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 041ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.66:1:1  -23.52:1:1 042ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 042ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 042ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.48:1:1  -23.52:1:1 043ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 043ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 043ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.52:1:1 044ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 044ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 044ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.52:1:1 045ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 045ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 045ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.4:1:1  -23.52:1:1 046ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 046ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 046ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.52:1:1 047ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 047ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 047ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.76:1:1  -23.5:1:1 048ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 048ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 048ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.74:1:1  -23.5:1:1 049ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 049ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 049ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.7:1:1  -23.5:1:1 050ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 050ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 050ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.5:1:1 051ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 051ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 051ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.5:1:1 052ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 052ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 052ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.36:1:1  -23.5:1:1 053ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 053ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 053ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.44:1:1  -23.48:1:1 054ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 054ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 054ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.42:1:1  -23.48:1:1 055ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 055ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 055ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.38:1:1  -23.48:1:1 056ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 056ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 056ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -23.24:1:1 057ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 057ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 057ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -45.8:1:1  -23.16:1:1 058ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 058ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 058ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.24:1:1  -23.12:1:1 059ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 059ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 059ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -23.0:1:1 060ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 060ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 060ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.0:1:1  -23.0:1:1 061ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 061ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 061ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.14:1:1  -22.98:1:1 062ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 062ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 062ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.18:1:1  -22.94:1:1 063ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 063ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 063ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.1:1:1  -22.92:1:1 064ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 064ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 064ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.92:1:1 065ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 065ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 065ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.9:1:1 066ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 066ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 066ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -22.9:1:1 067ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 067ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 067ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.22:1:1  -22.9:1:1 068ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 068ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 068ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.88:1:1 069ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 069ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 069ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.96:1:1  -22.88:1:1 070ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 070ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 070ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.86:1:1 071ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 071ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 071ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.04:1:1  -22.82:1:1 072ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 072ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 072ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.28:1:1  -22.8:1:1 073ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 073ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 073ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.16:1:1  -22.78:1:1 074ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 074ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 074ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.32:1:1  -22.74:1:1 075ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 075ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 075ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.06:1:1  -22.74:1:1 076ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 076ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 076ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.46:1:1  -22.48:1:1 077ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 077ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 077ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.02:1:1  -22.36:1:1 078ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 078ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 078ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.08:1:1  -22.32:1:1 079ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 079ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 079ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -22.28:1:1 080ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 080ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 080ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.82:1:1  -22.26:1:1 081ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 081ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 081ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -22.02:1:1 082ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 082ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 082ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.82:1:1 083ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 083ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 083ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.18:1:1  -21.8:1:1 084ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 084ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 084ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.08:1:1  -21.76:1:1 085ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 085ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 085ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.7:1:1  -21.58:1:1 086ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 086ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 086ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.12:1:1  -21.32:1:1 087ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 087ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 087ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.1:1:1  -21.3:1:1 088ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 088ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 088ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.42:1:1  -21.22:1:1 089ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 089ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 089ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.98:1:1  -21.14:1:1 090ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 090ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 090ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.42:1:1  -20.8:1:1 091ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 091ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 091ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -49.98:1:1  -20.44:1:1 092ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 092ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 092ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.3:1:1  -20.3:1:1 093ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 093ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 093ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -50.56:1:1  -20.26:1:1 094ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 094ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 094ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.02:1:1  -20.1:1:1 095ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 095ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 095ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -43.9:1:1  -19.96:1:1 096ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 096ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 096ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.82:1:1  -19.78:1:1 097ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 097ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 097ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -46.82:1:1  -19.46:1:1 098ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 098ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 098ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -47.9:1:1  -18.66:1:1 099ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 099ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 099ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.26:1:1  -18.64:1:1 100ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 100ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 100ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.22:1:1  -18.54:1:1 101ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 101ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 101ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -53.28:1:1  -17.84:1:1 102ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 102ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 102ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.16:1:1  -17.8:1:1 103ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 103ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 103ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -48.2:1:1  -17.66:1:1 104ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 104ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 104ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.22:1:1  -17.3:1:1 105ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 105ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 105ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -53.32:1:1  -17.26:1:1 106ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 106ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 106ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.66:1:1  -16.7:1:1 107ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 107ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 107ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -38.54:1:1  -13.02:1:1 108ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 108ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 108ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -54.6:1:1  -11.64:1:1 109ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 109ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 109ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -35.74:1:1  -9.64:1:1 110ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 110ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 110ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.44:1:1 111ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 111ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 111ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.94:1:1  -3.3:1:1 112ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 112ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 112ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.76:1:1  -3.3:1:1 113ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 113ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 113ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.74:1:1  -3.28:1:1 114ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 114ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 114ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.18:1:1 115ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 115ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 115ED_WRF_${date}${hr}_${date4}f${i}.csv &
   wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -lola  -51.78:1:1  -3.12:1:1 116ED_WRF_${date}${hr}_${date4}f${i}.grb grib
      wgrib2 116ED_WRF_${date}${hr}_${date4}f${i}.grb -csv 116ED_WRF_${date}${hr}_${date4}f${i}.csv &
cat *ED*.csv >> WRF_${date}${hr}_${date4}f${i}.csv
rm *.grib2
done


    