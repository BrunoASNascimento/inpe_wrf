#!/bin/bash
#
#define URL
#

cd /var/data/wrf

ano=$(date --date="-1 day" +'%Y')
mes=$(date --date="-1 day" +'%m')
dia=$(date --date="-1 day" +'%d')
hr=00

BASE_URL="http://ftp1.cptec.inpe.br/modelos/tempo/WRF/ams_05km/brutos/${ano}/${mes}/${dia}/${hr}/"

date=$(date --date="-1 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    #wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon -75:50:0.02 -30:35:0.02 ED_WRF_${date}${hr}_${date}f${i}.grib2
    #SE
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -33:875:0.02 ED_SE_WRF_${date}${hr}_${date}f${i}.grib2
    echo "---------- Parser to csv SE ----------"
    wgrib2 -match "surface" ./ED_SE_WRF_${date}${hr}_${date}f${i}.grib2 -csv WRF_SE_${date}${hr}_${date}f${i}.csv &
    #S
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -33:875:0.02 ED_S_WRF_${date}${hr}_${date}f${i}.grib2
    echo "---------- Parser to csv S ----------"
    wgrib2 -match "surface" ./ED_S_WRF_${date}${hr}_${date}f${i}.grib2 -csv WRF_S_${date}${hr}_${date}f${i}.csv &
    #NE
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -16.5:875:0.02 ED_NE_WRF_${date}${hr}_${date}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NE_WRF_${date}${hr}_${date}f${i}.grib2 -csv WRF_NE_${date}${hr}_${date}f${i}.csv &
    #N
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -16.5:875:0.02 ED_N_WRF_${date}${hr}_${date}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_N_WRF_${date}${hr}_${date}f${i}.grib2 -csv WRF_N_${date}${hr}_${date}f${i}.csv &
    #NO
    wgrib2 WRF_${date}${hr}_${date}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -75:700:0.02  -16.5:875:0.02 ED_NO_WRF_${date}${hr}_${date}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NO_WRF_${date}${hr}_${date}f${i}.grib2 -csv WRF_NO_${date}${hr}_${date}f${i}.csv &
    
done

python3 /var/scripts/inpe_wrf/main_normal.py &
python3 /var/scripts/inpe_wrf/main_reverse.py &

echo "---------- Zip file ----------"
tar -czf WRF_${date}${hr}_${date}.tar.gz *.grib2
rm *.grib2

echo "---------- Upload Zip file ----------"
gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 WRF_${date}${hr}_${date}.tar.gz &

date2=$(date +'%Y%m%d')
echo "$date2"
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date2}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date2}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    #wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon -75:50:0.02 -30:35:0.02 ED_WRF_${date}${hr}_${date2}f${i}.grib2
    #SE
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -33:875:0.02 ED_SE_WRF_${date}${hr}_${date2}f${i}.grib2
    echo "---------- Parser to csv SE ----------"
    wgrib2 -match "surface" ./ED_SE_WRF_${date}${hr}_${date2}f${i}.grib2 -csv WRF_SE_${date}${hr}_${date2}f${i}.csv &
    #S
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -33:875:0.02 ED_S_WRF_${date}${hr}_${date2}f${i}.grib2
    echo "---------- Parser to csv S ----------"
    wgrib2 -match "surface" ./ED_S_WRF_${date}${hr}_${date2}f${i}.grib2 -csv WRF_S_${date}${hr}_${date2}f${i}.csv &
    #NE
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -16.5:875:0.02 ED_NE_WRF_${date}${hr}_${date2}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NE_WRF_${date}${hr}_${date2}f${i}.grib2 -csv WRF_NE_${date}${hr}_${date2}f${i}.csv &
    #N
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -16.5:875:0.02 ED_N_WRF_${date}${hr}_${date2}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_N_WRF_${date}${hr}_${date2}f${i}.grib2 -csv WRF_N_${date}${hr}_${date2}f${i}.csv &
    #NO
    wgrib2 WRF_${date}${hr}_${date2}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -75:700:0.02  -16.5:875:0.02 ED_NO_WRF_${date}${hr}_${date2}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NO_WRF_${date}${hr}_${date2}f${i}.grib2 -csv WRF_NO_${date}${hr}_${date2}f${i}.csv &
done

echo "---------- Zip file ----------"
tar -czf WRF_${date}${hr}_${date2}.tar.gz *.grib2
rm *.grib2

echo "---------- Upload Zip file ----------"
gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 WRF_${date}${hr}_${date2}.tar.gz &

date3=$(date --date="+1 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date3}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date3}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    #wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon -75:50:0.02 -30:35:0.02 ED_WRF_${date}${hr}_${date3}f${i}.grib2
    #SE
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -33:875:0.02 ED_SE_WRF_${date}${hr}_${date3}f${i}.grib2
    echo "---------- Parser to csv SE ----------"
    wgrib2 -match "surface" ./ED_SE_WRF_${date}${hr}_${date3}f${i}.grib2 -csv WRF_SE_${date}${hr}_${date3}f${i}.csv &
    #S
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -33:875:0.02 ED_S_WRF_${date}${hr}_${date3}f${i}.grib2
    echo "---------- Parser to csv S ----------"
    wgrib2 -match "surface" ./ED_S_WRF_${date}${hr}_${date3}f${i}.grib2 -csv WRF_S_${date}${hr}_${date3}f${i}.csv &
    #NE
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -16.5:875:0.02 ED_NE_WRF_${date}${hr}_${date3}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NE_WRF_${date}${hr}_${date3}f${i}.grib2 -csv WRF_NE_${date}${hr}_${date3}f${i}.csv &
    #N
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -16.5:875:0.02 ED_N_WRF_${date}${hr}_${date3}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_N_WRF_${date}${hr}_${date3}f${i}.grib2 -csv WRF_N_${date}${hr}_${date3}f${i}.csv &
    #NO
    wgrib2 WRF_${date}${hr}_${date3}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -75:700:0.02  -16.5:875:0.02 ED_NO_WRF_${date}${hr}_${date3}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NO_WRF_${date}${hr}_${date3}f${i}.grib2 -csv WRF_NO_${date}${hr}_${date3}f${i}.csv &
done

echo "---------- Zip file ----------"
tar -czf WRF_${date}${hr}_${date3}.tar.gz *.grib2
rm *.grib2

echo "---------- Upload Zip file ----------"
gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 WRF_${date}${hr}_${date3}.tar.gz &

date4=$(date --date="+2 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date4}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date4}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
    #wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon -75:50:0.02 -30:35:0.02 ED_WRF_${date}${hr}_${date4}f${i}.grib2
    #SE
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -33:875:0.02 ED_SE_WRF_${date}${hr}_${date4}f${i}.grib2
    echo "---------- Parser to csv SE ----------"
    wgrib2 -match "surface" ./ED_SE_WRF_${date}${hr}_${date4}f${i}.grib2 -csv WRF_SE_${date}${hr}_${date4}f${i}.csv &
    #S
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -33:875:0.02 ED_S_WRF_${date}${hr}_${date4}f${i}.grib2
    echo "---------- Parser to csv S ----------"
    wgrib2 -match "surface" ./ED_S_WRF_${date}${hr}_${date4}f${i}.grib2 -csv WRF_S_${date}${hr}_${date4}f${i}.csv &
    #NE
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -45:700:0.02  -16.5:875:0.02 ED_NE_WRF_${date}${hr}_${date4}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NE_WRF_${date}${hr}_${date4}f${i}.grib2 -csv WRF_NE_${date}${hr}_${date4}f${i}.csv &
    #N
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -60:700:0.02  -16.5:875:0.02 ED_N_WRF_${date}${hr}_${date4}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_N_WRF_${date}${hr}_${date4}f${i}.grib2 -csv WRF_N_${date}${hr}_${date4}f${i}.csv &
    #NO
    wgrib2 WRF_${date}${hr}_${date4}f${i}.grib2 -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -75:700:0.02  -16.5:875:0.02 ED_NO_WRF_${date}${hr}_${date4}f${i}.grib2
    echo "---------- Parser to csv NE ----------"
    wgrib2 -match "surface" ./ED_NO_WRF_${date}${hr}_${date4}f${i}.grib2 -csv WRF_NO_${date}${hr}_${date4}f${i}.csv &
done

echo "---------- Zip file ----------"
tar -czf WRF_${date}${hr}_${date4}.tar.gz *.grib2
rm *.grib2

echo "---------- Upload Zip file ----------"
gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 WRF_${date}${hr}_${date4}.tar.gz
rm *.gz

echo "---------- Parser files ----------"
python3 /var/scripts/inpe_wrf/main.py
