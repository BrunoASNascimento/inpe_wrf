import pandas_gbq

sql = ("""SELECT distinct latitude_wrf, longitude_wrf FROM `pluvion-tech.stations.all_stations` order by 1,2""")
df = pandas_gbq.read_gbq(sql, project_id='pluvion-tech')

# print(df)
line_list = {
    'date': [],
    'date2': [],
    'date3': [],
    'date4': []
}

for index, row in df.iterrows():
    for type_date in line_list:
        # print(f'{type_date}')
        line = (
            '    wgrib2 WRF_${date}${hr}_${'
            + type_date + '}f${i}.grib2 -match "surface" -lola  '
            + str((row['longitude_wrf']))+':1:1  '
            + str((row['latitude_wrf']))+':1:1 '
            + str(index+1).zfill(3)
            + 'ED_WRF_${date}${hr}_${'+type_date +
            '}f${i}.grb grib\n        wgrib2 '
            + str(index+1).zfill(3)
            + 'ED_WRF_${date}${hr}_${'+type_date+'}f${i}.grb -csv '
            + str(index+1).zfill(3)
            + 'ED_WRF_${date}${hr}_${'+type_date+'}f${i}.csv &'
        )
        line_list[type_date].append(line)


line_str = "\n".join(line_list['date'])
line_str2 = "\n".join(line_list['date2'])
line_str3 = "\n".join(line_list['date3'])
line_str4 = "\n".join(line_list['date4'])


with open('get_inpe_00.sh', 'w') as get_inpe:
    get_inpe.write(
        """
#!/bin/bash
#
#define URL
#

cd /var/data/wrf

ano=$(date +'%Y')
mes=$(date +'%m')
dia=$(date +'%d')
hr=00

BASE_URL="http://ftp1.cptec.inpe.br/modelos/tempo/WRF/ams_05km/brutos/${ano}/${mes}/${dia}/${hr}/"

date=$(date +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date}${i}.grib2"
    echo "$URL"
    #Download file
    
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date}f${i}.grib2

    #Cut
    echo "---------- Cut file ----------"

        """
        + line_str +
        """

    rm *.grb 
    rm *.grib2
done


date2=$(date --date="+1 day" +'%Y%m%d')
echo "$date2"
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date2}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date2}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
        """
        + line_str2 +
        """
    rm *.grb 
    rm *.grib2
done


date3=$(date --date="+2 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date3}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date3}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
        """
        + line_str3 +
        """
    rm *.grb 
    rm *.grib2
done


date4=$(date --date="+3 day" +'%Y%m%d')
for i in {00..23..1}; do
    URL="${BASE_URL}WRF_cpt_05KM_${date}${hr}_${date4}${i}.grib2"
    echo "$URL"
    #Download file
    echo "---------- Download file ----------"
    curl "$URL" -o WRF_${date}${hr}_${date4}f${i}.grib2
    #Cut
    echo "---------- Cut file ----------"
        """
        + line_str4 +
        """
    rm *.grb 
    rm *.grib2
done
    """)
